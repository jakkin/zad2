## Lekcja 1 – Markdown lekki język znaczników

#### Spis treści

Lekcja 1 – Markdown lekki język znaczników....................................................................................1
Wstęp...............................................................................................................................................1
Podstawy składni.............................................................................................................................3
Definiowanie nagłówków...........................................................................................................3
Definiowanie list.........................................................................................................................4
Wyróżnianie tekstu......................................................................................................................4
Tabele..........................................................................................................................................5
Odnośniki do zasobów................................................................................................................5
Obrazki........................................................................................................................................5
Kod źródłowy dla różnych języków programowania.................................................................5
Tworzenie spisu treści na podstawie nagłówków.......................................................................6
Edytory dedykowane.......................................................................................................................7
Pandoc – system do konwersji dokumentów Markdown do innych formatów...............................8
Lekcja 2 – Git – system kontroli wersji................................................................................................9
Git - podstawowe cechy...................................................................................................................9
Idea pracy:........................................................................................................................................9
Git – tworzenie pustego archiwum lokalnego...............................................................................11
Zadania do wykonania na punkty.......................................................................................................21
Zadanie 1 – 2pkt............................................................................................................................21
Zadanie 2 – 4pkt............................................................................................................................21
Zadanie 3 - 4pkt.............................................................................................................................21

#### Wstęp

Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. *html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. *Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. *XML** (*Extensible Markup Language*) - uniwersalnym języku znaczników przeznaczonym do
   reprezentowania różnych danych w ustrukturalizowany sposób



W tym przypadku mamy np. znacznik np. <circle> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).
Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z
rozszerzeniem docx, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By
wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych
narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych
formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie
używany do tworzenia plików README.md (w projektach open source) i powszechnie
obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John
Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania
i tak w 2016 r. opublikowano dokument RFC 7764 który zawiera opis kilku odmian tegoż języka:

* Common Mark
* Github Flavoured Markdown
* Markdown Extra.